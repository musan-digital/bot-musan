# config valid only for current version of Capistrano
lock "3.9.1"

Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

set :application, "Bot_Musan"
set :repo_url, "git@bitbucket.org:musan-digital/bot-musan.git"

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

set :composer_download_url, "https://getcomposer.org/installer"

Rake::Task['deploy:updated'].prerequisites.delete('composer:install')

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :pty is false
set :pty, true


# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{composer.phar .env}

# Default value for linked_dirs is []
set :linked_dirs, %w{image-files runtime vendor public/uploads }

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
set :keep_releases, 5


namespace :deploy do

    desc "migrate db"
    task :migrate do
      on roles(:web) do
        execute "cd #{release_path}; ./yii migrate --interactive=0"
      end
    end

    desc "Install NPM packeges"
    task :npm_install do
      on roles(:web) do
        execute "cd #{release_path}; npm install --non-interactive --no-progress"
      end
    end

    desc "Build assets with gulp"
    task :gulp_build do
      on roles(:web) do
        execute "cd #{release_path}; gulp build"
      end
    end

    desc "Reload php-fpm"
    task :reload_fpm do
      on roles(:web) do
        execute "sudo systemctl reload php-fpm"
      end
    end

    desc "composer install"
    task :composer_install do
      on roles(:web) do
        execute "cd #{release_path}; php composer.phar self-update"
        execute "cd #{release_path}; php composer.phar install --no-dev --no-interaction --optimize-autoloader --no-progress"
      end
    end

     before :updated, 'deploy:composer_install'

     after :updated, 'deploy:migrate'

     # after :updated,  'deploy:npm_install'

     # after :updated,  'deploy:gulp_build'

     after :finished, 'deploy:reload_fpm'

end