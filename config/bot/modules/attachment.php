<?php
/**
 * Created by PhpStorm.
 * User: kocmokot
 * Date: 23.11.2017
 * Time: 13:56
 */
use SideKit\Config\ConfigKit;

return [
    'class' => 'mitrii\attachments\Module',
    'upload_path' => realpath(__DIR__ . '/../../../image-files/upload'),
    'upload_roles' => ['super-admin'],
    'components' => [
        'render' => [
            'class' => 'mitrii\attachments\components\RenderManager',
            'image_url_host' => ConfigKit::env()->get('HOST'),
            'secret' => 'fdsfdsafaotirjoiajdfzv8c09xzv8tooqi4jioflksdf',
            'placeholder_on_debug' => false,
        ],
    ],
    'show_options' => [
        'jpeg_quality' => 95,
        'png_compression_level' => 9
    ],
    'image_filter' => 'mitchell',
    'cache_resized' => true,
];