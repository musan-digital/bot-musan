<?php
/**
 * Created by PhpStorm.
 * User: kocmokot
 * Date: 10.02.2018
 * Time: 1:57
 */

return [
    'class' => 'developeruz\db_rbac\Yii2DbRbac',
    'layout' => '@app/views/layouts/admin',
    'params' => [
        'userClass' => 'app\modules\user\models\User',
        'accessRoles' => ['@']
    ]
];