<?php
/**
 * Created by PhpStorm.
 * User: kocmokot
 * Date: 16.02.2018
 * Time: 18:42
 */

return [
    [
        'text' => 'Регистрация',
        'route' => 'user/bot/register'
    ],
    [
        'text' => 'Выбрать услугу',
        'route' => 'keyboard/select'
    ],
    [
        'text' => 'HR',
        'route' => 'start/hr'
    ],
    [
        'text' => 'Отправить',
        'route' => 'task/default/send-email'
    ],
    [
        'text' => '⬅️ Назад',
        'route' => 'start/back',
    ],
    [
        'text' => 'Отменить',
        'route' => 'start/cancel',
    ],
    [
        'text' => 'Баннер',
        'route' => 'task/default/create',
    ],
    [
        'text' => 'Джингл',
        'route' => 'task/default/create',
    ],
    [
        'text' => 'Графика в соцсетях',
        'route' => 'task/default/create',
    ],
    [
        'text' => 'Продвижение в соцсетях',
        'route' => 'task/default/create',
    ],
    [
        'text' => 'Презентация',
        'route' => 'task/default/create',
    ],
    [
        'text' => 'Плакат',
        'route' => 'task/default/create',
    ],
    [
        'text' => 'Лонгрид',
        'route' => 'task/default/create',
    ],
    [
        'text' => 'Размещение баннера',
        'route' => 'task/default/create',
    ],
    [
        'text' => 'Размещение тендерных статей',
        'route' => 'task/default/create',
    ],
    [
        'text' => 'Завершить загрузку',
        'route' => 'task/default/upload-end',
    ],
];