<?php

return [
    'class' => 'yii\web\User',
    'identityClass' => '\app\modules\user\models\User',
    'enableSession' => false
];
