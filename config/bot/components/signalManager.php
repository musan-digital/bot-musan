<?php
/**
 * Created by PhpStorm.
 * User: kocmokot
 * Date: 31.01.2018
 * Time: 16:16
 */

return [
    'class' => mitrii\bot\SignalManager::className(),
    'rules' => [
        '/start' => 'start/index',
        '/choose' => 'task/default/choose',
        '/home' => 'start/home',
        'Завершить загрузку' => '/task/default/upload-end',
//        'Баннер' => 'task/default/create',
//        'Реклама' => 'task/default/create',
    ]
];