<?php
/**
 * Created by PhpStorm.
 * User: kocmokot
 * Date: 31.01.2018
 * Time: 16:15
 */

return [
    'class' => \yii\queue\redis\Queue::class,
    'redis' => 'queueRedis', // connection ID
    'channel' => 'updates', // queue channel
];