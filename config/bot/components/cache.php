<?php
/**
 * Created by PhpStorm.
 * User: kocmokot
 * Date: 31.01.2018
 * Time: 16:14
 */

return [

    /*
     * --------------------------------------------------------------------------
     * FileCache
     * --------------------------------------------------------------------------
     *
     * Implements a cache component using files.
     */
    'class' => 'yii\caching\FileCache',
];