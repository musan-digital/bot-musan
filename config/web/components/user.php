<?php

return [
    'identityClass' => 'app\modules\user\models\User',
    'enableAutoLogin' => true,
    'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
];
