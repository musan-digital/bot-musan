<?php
/**
 * Created by PhpStorm.
 * User: kocmokot
 * Date: 07.02.2018
 * Time: 22:54
 */


return [
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        '/' => 'admin/index',
        '<modules>/<action>' => '<modules>/admin/<action>'
    ],
];
