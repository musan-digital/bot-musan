<?php
/**
 * Created by PhpStorm.
 * User: kocmokot
 * Date: 31.01.2018
 * Time: 16:20
 */

return [
    'class' => yii\redis\Connection::class,
    'hostname' => 'localhost',
    'port' => 6379,
    'database' => 3,
];