<?php
/**
 * Created by PhpStorm.
 * User: kocmokot
 * Date: 31.01.2018
 * Time: 16:20
 */

use SideKit\Config\ConfigKit;

return [
    'class' => \mitrii\bot\BotManager::class,
    'botApp' => __DIR__ . '/../../../bootstrap/bot.php',
    'apiToken' => ConfigKit::env()->get('BOT_API_TOKEN'),
];