<?php

namespace app\modules\task\models;

use mitrii\attachments\behaviors\AttributesAttachmentBehavior;
use SideKit\Config\ConfigKit;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Json;

/**
 * This is the model class for table "check_lists".
 *
 * @property int $id
 * @property int $task_id
 * @property string $check_template
 * @property string $value
 * @property string $create_date
 * @property string $update_date
 * @property integer $is_files
 */
class CheckList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'check_lists';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id'], 'integer'],
            [['is_files'], 'boolean'],
            [['value'], 'string'],
            [['create_date', 'update_date'], 'safe'],
            [['check_template'], 'string', 'max' => 255],
        ];
    }


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date']
                ],
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => AttributesAttachmentBehavior::className(),
                'attributes' => [
                    'value',
                ],
            ],
        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_id' => 'Task ID',
            'check_template' => 'Check Template',
            'value' => 'Value',
            'create_date' => 'Create Date',
            'update_date' => 'Update Date',
        ];
    }

    /**
     * @inheritdoc
     * @return CheckListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CheckListQuery(get_called_class());
    }


    public function getFile()
    {
        $urls = [];

        foreach (Json::decode($this->value) as $value) {

            $attachment = \mitrii\attachments\models\Attachment::find($value)->where(['hash' => $value])->one();
            array_push($urls, ConfigKit::env()->get('HOST').'/uploads/'.$attachment->path);
        }
        return $urls;
    }
}
