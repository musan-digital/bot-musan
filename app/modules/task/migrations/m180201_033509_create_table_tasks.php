<?php

use yii\db\Migration;

/**
 * Class m180201_033509_create_table_tasks
 */
class m180201_033509_create_table_tasks extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('tasks', [
            'id' => $this->primaryKey(),
            'type' => $this->string(),
            'description' => $this->text(),
            'member' => $this->string(),
            'create_date' => $this->dateTime()->defaultValue(date('Y-m-d H:i:s')),
            'update_date' => $this->dateTime(),
            'is_completed' => $this->boolean()->defaultValue(0)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('tasks');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180201_033509_create_table_tasks cannot be reverted.\n";

        return false;
    }
    */
}
