<?php

use yii\db\Migration;

/**
 * Class m180212_043801_add_column_is_canceled_in_task
 */
class m180212_043801_add_column_is_canceled_in_task extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('tasks', 'is_canceled', $this->boolean()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('tasks', 'is_canceled');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180212_043801_add_column_is_canceled_in_task cannot be reverted.\n";

        return false;
    }
    */
}
