<?php

use yii\db\Migration;

/**
 * Class m180402_070943_add_column_is_files
 */
class m180402_070943_add_column_is_files extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('check_lists', 'is_files', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('check_lists', 'is_files');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180402_070943_add_column_is_files cannot be reverted.\n";

        return false;
    }
    */
}
