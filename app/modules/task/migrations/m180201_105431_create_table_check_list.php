<?php

use yii\db\Migration;

/**
 * Class m180201_105431_create_table_check_list
 */
class m180201_105431_create_table_check_list extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('check_lists', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer(),
            'check_template' => $this->string(),
            'value' => $this->text(),
            'create_date' => $this->dateTime(),
            'update_date' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('check_lists');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180201_105431_create_table_check_list cannot be reverted.\n";

        return false;
    }
    */
}
