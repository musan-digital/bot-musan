<?php

namespace app\modules\task\controllers;

use app\modules\task\models\CheckList;
use app\modules\task\models\Task;
use app\modules\user\models\LoginForm;
use app\modules\user\models\User;
use mitrii\attachments\models\Attachment;
use mitrii\bot\Controller;
use mitrii\bot\jobs\UpdateJob;
use SideKit\Config\ConfigKit;
use unreal4u\TelegramAPI\Telegram\Methods\GetFile;
use unreal4u\TelegramAPI\TgLog;
use Yii;
use yii\helpers\Json;

/**
 * Default controller for the `task` module
 */
class DefaultController extends Controller
{
    public $updates;

    public $task_id;

    public $member_id;

    public $chat_text;

    public function actions()
    {
        $this->layout = '@app/views/layouts/empty';
        $this->updates = \Yii::$app->request->update;
        $this->member_id = ($this->updates->message) ? $this->updates->message->from->id: $this->updates->callback_query->from->id;
        $this->chat_text = ($this->updates->message) ? $this->updates->message->text: $this->updates->callback_query;

        return parent::actions(); // TODO: Change the autogenerated stub
    }

    public function afterAction($action, $result)
    {
        $route = 'task/'.Yii::$app->controller->id.'/'.Yii::$app->controller->action->id;
        $cache = Yii::$app->cache;

        if ($route === 'task/default/create') {
            $route = 'task/default/check-list-add';
        }
        elseif ($route === 'task/default/upload-end') {
            $route = 'task/default/check-list-add';
        }

        $cache->set($this->member_id.'-signal-route', $route);
        return parent::afterAction($action, $result); // TODO: Change the autogenerated stub
    }


    public function actionCreate()
    {
        $cache = Yii::$app->cache;
        $task = new Task();
        $task->type = Task::TASK_TYPES[$this->updates->message->text];
        $task->is_completed = 0;
        $task->member = (string)$this->updates->message->from->id;

        if ($task->save())
        {
            $cache->set($this->member_id.'-task-id', $task->id);
            $cache->set($this->member_id.'-check-type', $this->chat_text);
            $cache->set($this->member_id.'-check-step', 1);

            $text = $this->render(Task::TASK_TYPES[$this->updates->message->text]);

            \Yii::$app->queue->delay(0)->push(new UpdateJob([
                'update' => $this->updates,
                'route' => ['keyboard/cancel', ['text' => $text]]
            ]));

            \Yii::$app->queue->delay(0)->push(new UpdateJob([
                'update' => $this->updates,
                'route' => ['/message/check-answer', ['type' => $this->chat_text, 'step' => 0]]
            ]));

        }

        return;

    }

    public function actionCheckListAdd()
    {
        $cache = Yii::$app->cache;

        $check_step = $cache->get($this->member_id.'-check-step');
        $check_type = $cache->get($this->member_id.'-check-type');

        $check_lists_template = Task::CHECK_LISTS[Task::TASK_TYPES[$check_type]];


        if ($this->updates->message->document || $this->updates->message->photo) {

            return $this->run('/task/default/upload-files', ['check_lists_template' => $check_lists_template[$check_step-1]]);
        }

        if ($check_step <= count($check_lists_template))
        {
            $i = $check_step;

            $check_list = new CheckList();
            $check_list->task_id = $cache->get($this->member_id.'-task-id');
            $check_list->check_template = Task::CHECK_LISTS[Task::TASK_TYPES[$check_type]][$i-1];
            $check_list->value = self::convertValue($this->updates->message);

            $check_list->save();

            $cache->set($this->member_id.'-check-step', $check_step + 1);
        }

        if ($check_step >= count(Task::CHECK_LISTS[Task::TASK_TYPES[$check_type]]))
        {
            $cache->delete($this->member_id.'-check-step');
            $cache->delete($this->member_id.'-check-type');

            return $this->run('/keyboard/send-email');
        }

        return $this->run('/message/check-answer', ['type' => $check_type, 'step' => $check_step]);
    }

    public function actionUploadFiles($check_list_template = null)
    {
        $message = $this->updates->message;
        $cache = Yii::$app->cache;

        $attachment_array = $cache->get($this->member_id.'-attachments');
        $task_id = $cache->get($this->member_id.'-task-id');

        if ($attachment_array == false) {
            $check_list = new CheckList();
            $check_list->task_id = $task_id;
            $check_list->check_template = (string)$check_list_template;
            $check_list->is_files = true;

            $check_list->save();
        }

        $tgLog = new TgLog(ConfigKit::env()->get('BOT_API_TOKEN'));

        $getFile = new GetFile();
        $getFile->file_id = ($message->document) ? $message->document->file_id : $message->photo[3]->file_id;

        $file = $tgLog->performApiRequest($getFile);

        if ($file) {
            $value =  'https://api.telegram.org/file/bot'.ConfigKit::env()->get('BOT_API_TOKEN').'/'.$file->file_path;

            if (!(new \yii\validators\UrlValidator())->validate($value, $error)) return $value;

            $file = new \mitrii\attachments\components\AttachmentFile();

            $url_filename = parse_url($value, PHP_URL_PATH);
            $tmp_filename = explode(".", $url_filename);
            $file_ext = array_pop($tmp_filename);

            $path_dir = realpath(dirname(__FILE__).'/../../../../public/uploads/').'/';
            $filename = $path_dir . '/' . $file->generateSavePath($path_dir . '/' , 5, $file_ext);

            copy($value, $filename);

            $attachment = new Attachment();
            $attachment->original_name = basename($url_filename);
            $attachment->hash = $file->getHash();
            $attachment->path = $file->getSavePath();

            $attachment->type = \yii\helpers\FileHelper::getMimeType($filename);
            $attachment->size = filesize($filename);

            if ($attachment->save()) {
                $attachment_array = (is_array($attachment_array)) ? $attachment_array : [];

                array_push($attachment_array, $attachment->hash);
                $cache->set($this->member_id . '-attachments', $attachment_array);
            }

        }

        if ($attachment_array) {
            $check_list = CheckList::find()->where(['task_id' => $task_id])->orderBy('id DESC')->one();

            $check_list->value = Json::encode($attachment_array);

            $check_list->update();
            return $this->run('/keyboard/upload-end');
        }

    }

    public function actionUploadEnd()
    {
        $cache = Yii::$app->cache;

        $check_step = $cache->get($this->member_id . '-check-step');
        $check_type = $cache->get($this->member_id.'-check-type');

        $cache->delete($this->member_id . '-attachments');
        $cache->delete($this->member_id . '-check-list-template');

        $cache->set($this->member_id . '-check-step', $check_step+1);

        \Yii::$app->queue->delay(0.1)->push(new UpdateJob([
            'update' => $this->updates,
            'route' => ['/message/check-answer', ['type' => $check_type, 'step' => $check_step]]
        ]));

        return $this->run('/task/default/check-list-add');

    }

    public static function convertValue($message)
    {

        if (empty($message)) return false;

        if ($message->photo == true)
        {
            if( $curl = curl_init() ) {
                curl_setopt($curl, CURLOPT_URL, 'https://api.telegram.org/bot'.ConfigKit::env()->get('BOT_API_TOKEN').'/getFile?file_id='.$message->photo[0]->file_id);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
                curl_setopt($curl, CURLOPT_POST, true);
                $file_path = curl_exec($curl);
                curl_close($curl);
                $file_path = Json::decode($file_path);
                if ($file_path['result']['file_path'])
                {
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, 'https://api.telegram.org/file/bot'.ConfigKit::env()->get('BOT_API_TOKEN').'/'.$file_path['result']['file_path']);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
                    curl_setopt($curl, CURLOPT_POST, true);
                    $file = curl_exec($curl);
                    $info = curl_getinfo($curl);
                    curl_close($curl);
                }
                return $info['url'];
            }
        }
        elseif ($message->document) {
            if( $curl = curl_init() ) {
                curl_setopt($curl, CURLOPT_URL, 'https://api.telegram.org/bot'.ConfigKit::env()->get('BOT_API_TOKEN').'/getFile?file_id='.$message->document->file_id);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
                curl_setopt($curl, CURLOPT_POST, true);
                $file_path = curl_exec($curl);
                curl_close($curl);
                $file_path = Json::decode($file_path);
                if ($file_path['result']['file_path'])
                {
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, 'https://api.telegram.org/file/bot'.ConfigKit::env()->get('BOT_API_TOKEN').'/'.$file_path['result']['file_path']);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
                    curl_setopt($curl, CURLOPT_POST, true);
                    $file = curl_exec($curl);
                    $info = curl_getinfo($curl);
                    curl_close($curl);
                }
                return $info['url'];
            }


        }
        else {
            return $message->text;
        }
    }

    public function actionSendEmail()
    {
        $cache = Yii::$app->cache;

        $task = Task::find()->where(['id' => $cache->get($this->member_id.'-task-id')])->one();
        $user = User::find()->where(['username' => (string)$this->member_id])->one();

        $subject = array_search($task->type, \app\modules\task\models\Task::TASK_TYPES);

        $check_list = CheckList::find()->where(['task_id' => $task->id])->all();

        Yii::$app->mailer->compose('html',['task' => $task, 'check_list' => $check_list, 'user' => $user]) // a view rendering result becomes the message body here
        ->setFrom(['bot@musan.kz' => 'Musan Group BOT'])
            ->setTo(Task::EMAILS[$task->type])///
            ->setSubject('Новая заявка на '.$subject . ' от '.$user->profile->fio)
            ->send();

        Yii::$app->mailer->compose('html-copy',['task' => $task, 'check_list' => $check_list, 'user' => $user]) // a view rendering result becomes the message body here
        ->setFrom(['bot@musan.kz' => 'Musan Group BOT'])
            ->setTo($user->email)
            ->setSubject('Копия заявки на '.$subject)
            ->send();

        return $this->run('/keyboard/home', ['type' => 'send-email']);
    }
}