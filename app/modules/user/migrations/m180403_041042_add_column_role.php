<?php

use yii\db\Migration;

/**
 * Class m180403_041042_add_column_role
 */
class m180403_041042_add_column_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'role', $this->string()->defaultValue('member'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'role');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180403_041042_add_column_role cannot be reverted.\n";

        return false;
    }
    */
}
