<?php

use yii\db\Migration;

/**
 * Class m180223_042717_addd_phone_column
 */
class m180223_042717_addd_phone_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profiles', 'phone', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('profiles', 'phone');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180223_042717_addd_phone_column cannot be reverted.\n";

        return false;
    }
    */
}
