<?php

use yii\db\Migration;

/**
 * Class m180219_054134_create_table_profile_user
 */
class m180219_054134_create_table_profile_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('profiles', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'fio' => $this->string(),
            'company' => $this->string(),
            'position' => $this->string()
        ]);

        $this->createIndex('user_id', 'profiles', 'user_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('profiles');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180219_054134_create_table_profile_user cannot be reverted.\n";

        return false;
    }
    */
}
