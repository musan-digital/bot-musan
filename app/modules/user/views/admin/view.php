<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('db_rbac', 'Управление ролями пользователя') .' '. $user->getUserName();
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Пользователи'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-form box box-primary">
    <div class="box-header with-border">
        <h1><?= $this->title; ?></h1>
        <?= Breadcrumbs::widget([
            'options' => ['class' => 'breadcrumb'],
            'links' => $this->params['breadcrumbs'],
        ]) ?>
    </div>
    <?php $form = ActiveForm::begin(); ?>
        <div class="box-body table-responsive">
            <?= Html::checkboxList('roles', $user_permit, $roles, ['separator' => '<br>']); ?>
        </div>

        <div class="box-footer">
            <?= Html::submitButton(Yii::t('db_rbac', 'Сохранить'), ['class' => 'btn btn-success']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
