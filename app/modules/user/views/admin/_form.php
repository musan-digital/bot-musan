<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="page-form box box-primary">
    <div class="box-header with-border">
        <h1><?= Html::encode($this->title) ?></h1>
        <?= Breadcrumbs::widget([
            'options' => ['class' => 'breadcrumb'],
            'links' => $this->params['breadcrumbs'],
        ]) ?>
    </div>
    <?php $form = ActiveForm::begin(); ?>

    <div class="box-body table-responsive">
        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'email')->textInput() ?>

        <?= $form->field($model, 'password')->passwordInput() ?>
    </div>

    <div class="box-footer">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-success btn-flat', 'name' => 'signup-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
