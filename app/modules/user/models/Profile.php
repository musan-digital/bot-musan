<?php

namespace app\modules\user\models;

use Yii;

/**
 * This is the model class for table "profiles".
 *
 * @property integer $id
 * @property int $user_id
 * @property string $fio
 * @property string $company
 * @property string $position
 * @property string $phone
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profiles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'id'], 'integer'],
            [['fio', 'company', 'position', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь ID',
            'fio' => 'ФИО',
            'company' => 'Компания',
            'position' => 'Должность',
            'phone' => 'Телефон',
        ];
    }

    /**
     * @inheritdoc
     * @return ProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProfileQuery(get_called_class());
    }
}
