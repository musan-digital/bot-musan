<?php

namespace app\controllers;

use app\models\ContactForm;
use app\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;

class AdminController extends Controller
{
    public $breadcrumbs = [];

    public function behaviors()
    {
        return [
            'as AccessBehavior' => [
                'class' => \developeruz\db_rbac\behaviors\AccessBehavior::className(),
                'redirect_url' => Url::to(['/user/admin/login']),
                'login_url' => Url::to(['/user/admin/login'])
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->breadcrumbs = array_merge($this->breadcrumbs, ['label' => '']);

        return $this->render('index', ['breadcrumbs' => $this->breadcrumbs]);
    }

    public function actionAbout()
    {
        $this->breadcrumbs = array_merge($this->breadcrumbs, ['label' => 'О нас']);
        return $this->render('about', ['breadcrumbs' => $this->breadcrumbs]);
    }
}
