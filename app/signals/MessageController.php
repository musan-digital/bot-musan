<?php
/**
 * Created by PhpStorm.
 * User: kocmokot
 * Date: 16.02.2018
 * Time: 16:52
 */

namespace app\signals;


use app\modules\task\models\Task;
use mitrii\bot\Controller;
use mitrii\bot\View;

class MessageController extends Controller
{
    public function actions()
    {
        $this->layout = 'empty';

        return parent::actions(); // TODO: Change the autogenerated stub
    }

    public function actionIndex($type = null)
    {
        return $this->render($type);
    }

    public function actionCheckAnswer($type = null, $step = 0)
    {
        $type = Task::TASK_TYPES[$type];

        $check_list = Task::CHECK_LISTS[$type][$step];
        if (is_array($check_list))
        {
            return (string)$check_list['title'];
        }
        else {
            return (string)$check_list;
        }
    }
}