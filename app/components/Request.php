<?php

namespace app\components;

use unreal4u\TelegramAPI\Telegram\Types\Message;
use unreal4u\TelegramAPI\Telegram\Types\Update;
use Yii;
use yii\base\Exception;

/**
 * Class Request
 * @package mitrii\bot
 * @property Message $message
 */
class Request extends \yii\web\Request
{

    const TYPE_MESSAGE = 'MESSAGE';
    const TYPE_CALLBACK_QUERY = 'CALLBACK_QUERY';

    /**
     * @var unreal4u\TelegramAPI\Telegram\Types\Update
     */
    public $update;

    public $route = false;

    public function getType()
    {
        if (!empty($this->update->message)) return 'MESSAGE';
        if (!empty($this->update->callback_query)) return 'CALLBACK_QUERY';
    }

    /**
     * Resolves the current request into a route and the associated parameters.
     * @return array the first element is the route, and the second is the associated parameters.
     */
    public function resolve()
    {

        if ($this->route !== false) return $this->route;
        $result = Yii::$app->getSignalManager()->parseRequest($this);

        return $result;
        /*
        if ($result !== false) {
            list ($route, $params) = $result;
            if ($this->_queryParams === null) {
                $_GET = $params + $_GET; // preserve numeric keys
            } else {
                $this->_queryParams = $params + $this->_queryParams;
            }
            return [$route, $this->getQueryParams()];
        } else {
        */
            throw new Exception(Yii::t('yii', 'Command not found.'));
        /*
        }
        */
    }

    /**
     * @return unreal4u\TelegramAPI\Telegram\Types\Update
     */
    public function getUpdate()
    {
        return $this->update;
    }

    /**
     * @param $update Update
     */
    public function setUpdate($update)
    {
        $this->update = $update;
    }


    public function getCustomRoute()
    {
        return $this->route;
    }

    public function setCustomRoute($route)
    {
        $this->route = $route;
    }

    public function getChatId()
    {
        return ($this->message !== false) ? $this->message->chat->id : false;
    }

    public function getChat()
    {
        return ($this->message !== false) ? $this->message->chat : false;
    }

    /**
     * @return bool|Message
     */
    public function getMessage()
    {
        switch ($this->type) {
            case self::TYPE_MESSAGE:
                return $this->update->message;
            case self::TYPE_CALLBACK_QUERY:
                return $this->update->callback_query->message;
            default:
                return false;
        }
    }

    public function getUser()
    {
        switch ($this->type) {
            case self::TYPE_MESSAGE:
                return $this->update->message->from;
            case self::TYPE_CALLBACK_QUERY:
                return $this->update->callback_query->from;
            default:
                return false;
        }
    }

}
