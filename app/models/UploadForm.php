<?php
/**
 * Created by PhpStorm.
 * User: kocmokot
 * Date: 31.03.2018
 * Time: 0:41
 */

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false],
        ];
    }

//, 'extensions' => 'png, jpg, '

    public function upload()
    {
        if ($this->validate()) {
            $this->imageFile->saveAs(realpath(dirname(__FILE__).'/image-files/uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension));
            return true;
        } else {
            return false;
        }
    }
}