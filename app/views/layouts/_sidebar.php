<?php

?>

<!-- Sidebar user panel -->
<div class="user-panel">
    <div class="pull-left image">
        <?php use yii\helpers\Url;

        echo \cebe\gravatar\Gravatar::widget(
            [
                'email' => 'username@example.com',
                'options' => [
                    'alt' => 'username',
                ],
                'size' => 64,
            ]
        ); ?>
    </div>
    <div class="pull-left info">
        <p><?= Yii::$app->getUser()->identity->username ?></p>

        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
</div>

<?php /*
<!-- search form -->
<form action="#" method="get" class="sidebar-form">
    <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search..."/>
        <span class="input-group-btn">
            <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i
                        class="fa fa-search"></i></button>
        </span>
    </div>
</form>
<!-- /.search form -->
*/ ?>

<?php

// prepare menu items, get all modules
$menuItems = [];

$userItems[] = [
    'url' => Url::to(['/user/admin/index']),
    'label' => 'Пользователи',
];
$userItems[] = [
    'url' => Url::to(['/permit/access/role']),
    'label' => 'Роли'
];
$userItems[] = [
    'url' => Url::to(['/permit/access/permission']),
    'label' => 'Права доступа'
];

$menuItems[] = [
    'url' => '#',
    'icon' => 'user',
    'label' => 'Пользователи',
    'items' => $userItems
];




echo dmstr\widgets\Menu::widget([
    'items' => $menuItems,
]);
?>
